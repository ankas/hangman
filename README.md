## Requirements

* Python 2.7
* virtualenv

### Setup

    mkvirtualenv hangman
    pip install -r requirements/base.txt

## Testing

Install extra dependencies required by the tests:

    pip isntall -r requirements/development.txt

Then run the test suite:
    
    python -m unittest discover .

Or with coverage:

    coverage run -m unittest discover ; coverage report

## Running the app

To run the Flask application:

    FLASK_DEBUG=1 FLASK_APP=app.py flask run

And then visit `http://127.0.0.1:5000/` in your favourite browser.

## Notes on the implementation

* A simple Flask app that provides an interface to the game seemed like a fitting choice;

* Game state is stored in a session, without any persistent backend;

* Classic hangman doesn't allow player to make same incorrect guesses multiple times, but implementing this seems to be out of scope, so submitting a wrong letter again *will* count as another mistake;
