from hangman import HangmanGame
import pickle
from flask import Flask
from flask import render_template, request, session, jsonify, redirect
app = Flask(__name__)


@app.route('/')
def index():
    game = HangmanGame()
    session['game'] = pickle.dumps(game)
    return render_template('game.html', game=game.to_dict())


@app.route('/guess/', methods=['POST'])
def guess():
    game = pickle.loads(session['game'])
    game.guess(request.get_json()['letter'])
    # make sure it's the updated game that's saved
    session['game'] = pickle.dumps(game)
    return jsonify(game.to_dict())


@app.route('/win/')
def win():
    game = 'game' in session and pickle.loads(session['game'])
    if not game or not game.status:
        return redirect('/')
    del session['game']
    return render_template('win.html', game=game.to_dict())


@app.route('/gameover/')
def gameover():
    game = 'game' in session and pickle.loads(session['game'])
    if not game or game.status is not False:
        return redirect('/')
    del session['game']
    return render_template('gameover.html', game=game.to_dict(), word=game.word)


if __name__ == '__main__':
    app.run()
app.secret_key = 'notasecretno'
