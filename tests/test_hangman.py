from hangman import HangmanGame
from unittest import TestCase
import mock
_mock_game_config = {
    '__len__.return_value': 1,
    '__getitem__.return_value': 'test'}


class TestHangmanGame(TestCase):
    @mock.patch('hangman.HangmanGame.words', **_mock_game_config)
    def setUp(self, mock_words):
        self.game = HangmanGame()

    def test_game_over_sets_status(self):
        self.assertIsNone(self.game.status)
        [self.game.guess(_) for _ in 'abcdz']
        self.assertFalse(self.game.status)

    def test_misses_are_counted(self):
        initial_misses = self.game.misses
        self.game.guess('z')
        self.assertEqual(self.game.misses, initial_misses - 1)

    def test_win_sets_status(self):
        self.assertIsNone(self.game.status)
        [self.game.guess(_) for _ in 'tes']
        self.assertTrue(self.game.status)

    def test_empty_guess_no_game_state_change(self):
        initial_misses = self.game.misses
        initial_guessed = self.game.guessed
        initial_state = self.game.status
        self.game.guess('')
        self.assertEqual(
            (initial_misses, initial_guessed, initial_state),
            (self.game.misses, self.game.guessed, self.game.status))
