from unittest import TestCase
import app
import mock
import hangman
import json


class AppTestCase(TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    @mock.patch('app.session')
    @mock.patch('app.pickle.dumps', return_value='pickled')
    def test_main_view_saves_game_state(self, mock_pickle, mock_session):
        self.app.get('/')
        mock_session.__setitem__.assert_called_once_with('game', 'pickled')

    def test_win_view_redirects_if_no_game(self):
        rv = self.app.get('/win/')
        self.assertEqual(rv.status_code, 302)
        self.assertEqual(rv.location, 'http://localhost/')

    def test_gameover_view_redirects_if_no_game(self):
        rv = self.app.get('/gameover/')
        self.assertEqual(rv.status_code, 302)
        self.assertEqual(rv.location, 'http://localhost/')

    @mock.patch('app.session')
    @mock.patch('app.pickle.loads')
    def test_gameover_view_redirects_if_won(self, mock_pickle, mock_session):
        game = hangman.HangmanGame()
        game.status = True
        mock_pickle.return_value = game
        mock_session.__getitem__.return_value = ''

        rv = self.app.get('/gameover/')

        self.assertEqual(rv.status_code, 302)
        self.assertEqual(rv.location, 'http://localhost/')

    @mock.patch('app.session')
    @mock.patch('app.pickle.loads')
    @mock.patch('hangman.HangmanGame.words')
    def test_guess_returns_guessed_letters(self, mock_words, mock_pickle, mock_session):
        mock_words.__getitem__.return_value = 'test'
        mock_words.__len__.return_value = 1
        game = hangman.HangmanGame()
        mock_pickle.return_value = game
        mock_session.__getitem__.return_value = ''

        rv = self.app.post('/guess/', data=json.dumps({'letter': 't'}), content_type='application/json')

        self.assertEqual(rv.status_code, 200)
        self.assertEqual(rv.data, '{\n  "guessed": "t__t", \n  "misses": 5, \n  "status": null\n}\n')

    @mock.patch('app.session')
    @mock.patch('app.pickle.loads')
    def test_gameover_view_clears_game_state(self, mock_pickle, mock_session):
        mock_session.__contains__.return_value = True
        game = hangman.HangmanGame()
        game.status = False
        mock_pickle.return_value = game

        rv = self.app.get('/gameover/')

        self.assertEqual(rv.status_code, 200)
        mock_session.__delitem__.assert_called_once_with('game')

    @mock.patch('app.session')
    @mock.patch('app.pickle.loads')
    def test_win_view_clears_game_state(self, mock_pickle, mock_session):
        mock_session.__contains__.return_value = True
        game = hangman.HangmanGame()
        game.status = True
        mock_pickle.return_value = game

        rv = self.app.get('/win/')

        self.assertEqual(rv.status_code, 200)
        mock_session.__delitem__.assert_called_once_with('game')
