import random
import re


class HangmanGame(object):
    words = ('3dhubs', 'marvin', 'print', 'filament', 'order', 'layer')

    def __init__(self):
        self.word = self.words[random.randint(0, len(self.words) - 1)]
        self.l = len(self.word)
        self.misses = 5  # mistakes left, game is over when this counter reaches 0
        self.guessed = set()  # holds positions of the letters that have been guessed already
        self.status = None  # True if the game is won, False if lost, None if unfinished

    def guess(self, letter):
        """
        Expects a letter, returns a list of indices that correspond to locations
        of this letter in the word.

        :param letter: a letter
        :type letter: str
        :return: a list of positions this letter appears at in the word
        :rtype: list of int
        """
        if not letter:
            return []
        found = [_.start() for _ in re.finditer(letter, self.word)]
        if not found:
            self.misses = max(0, self.misses - 1)
            if not self.misses:
                self.status = False
        self.guessed.update(found)
        if len(self.guessed) == self.l:
            self.status = True
        return found

    def to_dict(self):
        """
        Returns a dict that holds the state of the game and can be used in templates
        or for serialisation to JSON
        """
        return {
            'misses': self.misses, 'status': self.status,
            'guessed': ''.join([self.word[i] if i in self.guessed else '_'
                                for i in xrange(self.l)])}
